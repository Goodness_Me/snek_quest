import player
import character
from things import MyThing

PLAYER = player.Player()  # a fresh instance of the game player object

character.EngineReference.pr = PLAYER
MyThing.er = PLAYER  # set the engine reference for all newly created objects

myid = 55
name = "John"
CHARACTER = character.Character()
CHARACTER.discord_id = myid
CHARACTER.name = name
CHARACTER.random_abilities()

PLAYER.known_characters = {55:CHARACTER}
first_output = PLAYER.start_game(myid)

command = None

print(first_output)
while not CHARACTER.dead:
    command = input(">")
    output = PLAYER.process_command(command, myid)
    print("----------")
    print(output)
