import player
import character
from things import MyThing


class QuestRunner:

    def __init__(self):

        self.PLAYER = player.Player()
        character.EngineReference.pr = self.PLAYER
        MyThing.er = self.PLAYER  # set the engine reference for all newly created objects

        self.known_users = []
        self.PLAYER.known_characters = {}
        ### replace with database loading code or something
        self.currently_playing = []

    def make_test_character(self, discord_id, name):

        char = character.Character()
        char.discord_id = discord_id
        char.name = name
        #self.PLAYER.current_character = char
        char.random_abilities()
        #self.PLAYER.current_character = None

        return char

    def quest_start(self, discord_id):

        first_output = self.PLAYER.start_game(discord_id)
        return first_output

    def process_command(self, command, discord_id):

        if discord_id in self.currently_playing:
            return self.PLAYER.process_command(command, discord_id)
        else:
            self.currently_playing.append(discord_id)
            return self.quest_start(discord_id)

    def register_user(self, discord_id, name):

        self.known_users.append(discord_id)
        self.PLAYER.known_characters[discord_id] = self.make_test_character(discord_id, name)
        print("registered user {}".format(name))
